module gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.1
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.29.1
)

go 1.13
